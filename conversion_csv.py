"""
    The ``conversion_csv`` module
    ==============================

    Un simple script pour la conversion de fichier CSV vers un autre format de
    fichier CSV.

"""

import csv
import argparse
import datetime


def lectureFichier(fichier, delimiter):
    """
    Lit le fichier passé en paramètre.

    :param fichier: lien du fichier
    :type fichier: str
    :param delimiter: séparateur entre les champs
    :type delimiter: str
    :return: liste de dictionnaire
    :rtype: list
    """
    liste = []
    with open(fichier) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=delimiter)
        for row in reader:
            liste.append(row)
    return liste


def ecritureFichier(liste, fieldnames, fichier_modif, delimiter):
    """
    Ecrit dans un fichier les informations contenues dans la liste.

    :param liste: la liste avec les informations
    :type liste: list
    :param fieldnames: ordre des champs pour l'écriture
    :type fieldnames: dict
    :param fichier_modif: lien vers le fichier qui sera écrit
    :type fichier_modif: str
    :param delimiter: séparateur entre les champs
    :type delimiter: str

    """
    with open(fichier_modif, 'w', newline='') as csvfile:
        writer = csv.DictWriter(
            csvfile, fieldnames=fieldnames, delimiter=delimiter)

        writer.writeheader()
        for element in liste:
            writer.writerow(element)


def separationTypeVarianteVersion(liste):
    """
    Sépare le type, la variante et la version contenus dans une chaîne de caractères.

    :param liste: liste contenant des dictionnaires avec les valeurs à modifier
    :type liste: list
    """
    for element in liste:
        type_variante_version = element.get("type_variante_version")
        tab_split = type_variante_version.split(', ')
        element.update({"type": tab_split[0]})
        element.update({"variante": tab_split[1]})
        element.update({"version": tab_split[2]})
        element.pop("type_variante_version")


def renommageChamps(liste, dic_config):
    """
    Rennome les champs en suivant le dictionnaire de configuration passé
    en paramètre.

    :param liste: liste avec les champs originaux
    :type liste: list
    :param dic_config: dictionnaire avec les équivalences
    :type dic_config: dict
    :return: dictionnaire avec les clés modifiées
    :rtype: list
    """
    new_liste = []
    for element in liste:
        new_dic = {}
        for value in element.keys():
            new_dic[dic_config.get(value)] = element.get(value)
        new_liste.append(new_dic)
    return new_liste


def changeFormatDate(liste):
    """
    Change le format de la date.

    :param liste: liste avec les dates à modifier
    :type liste: list
    """
    for element in liste:
        date = datetime.datetime.strptime(
            element.get("date_immat"), '%Y-%m-%d')
        element.update({'date_immat': date.strftime('%d/%m/%Y')})


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        "Modifie l'ordre des élements du fichier CSV ainsi que le délimiteur")
    parser.add_argument("fichier_source", help="Le fichier à modifier.")
    parser.add_argument("fichier_dest", help="Le fichier modifié.")
    parser.add_argument("separateur", help="Le séparateur du fichier modifié.")
    args = parser.parse_args()

    # Correspondance des champs
    config = {'address': 'address_titulaire', 'carrosserie': 'carrosserie',
              'categorie': 'categorie', 'couleur': 'couleur',
              'cylindree': 'cylindree', 'date_immat': 'date_immatriculation',
              'denomination': 'denomination_commerciale', 'energy': 'energie',
              'firstname': 'prenom', 'immat': 'immatriculation',
              'marque': 'marque', 'name': 'nom', 'places': 'places',
              'poids': 'poids', 'puissance': 'puissance', 'type': 'type',
              'variante': 'variante', 'version': 'version', 'vin': 'vin'}

    # Ordre des champs
    champs = ['address_titulaire', 'nom', 'prenom', 'immatriculation',
              'date_immatriculation', 'vin', 'marque',
              'denomination_commerciale', 'couleur', 'carrosserie',
              'categorie', 'cylindree', 'energie', 'places',
              'poids', 'puissance', 'type', 'variante', 'version']

    liste_source = lectureFichier(args.fichier_source, '|')

    separationTypeVarianteVersion(liste_source)

    changeFormatDate(liste_source)

    liste_renom = renommageChamps(liste_source, config)

    ecritureFichier(liste_renom, champs, args.fichier_dest, args.separateur)
