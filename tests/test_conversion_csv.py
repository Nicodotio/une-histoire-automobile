import unittest

from conversion_csv import lectureFichier, ecritureFichier
from conversion_csv import separationTypeVarianteVersion, renommageChamps
from conversion_csv import changeFormatDate


class TestConversionCsvFunctions(unittest.TestCase):
    def test_lecture_fichier(self):
        result = [{'firstname': 'Nicolas', 'lastname': 'FONTAINE'},
                  {'firstname': 'Thomas', 'lastname': 'NOVAREZE'}]
        self.assertEqual(lectureFichier(
            './tests/fichier_test.csv', ','), result)

    def test_ecriture_fichier(self):
        liste_source = [{'firstname': 'Nicolas', 'lastname': 'FONTAINE'},
                        {'firstname': 'Thomas', 'lastname': 'NOVAREZE'}]
        champs = ['lastname', 'firstname']

        ecritureFichier(liste_source, champs,
                        './tests/fichier_test_modif.csv', ';')

        result = [{'lastname': 'FONTAINE', 'firstname': 'Nicolas'},
                  {'lastname': 'NOVAREZE', 'firstname': 'Thomas'}]
        self.assertEqual(lectureFichier(
            './tests/fichier_test_modif.csv', ';'), result)

    def test_separation_type_variante_version(self):
        liste_source = [{'type_variante_version': 'Inc, 92-3625175, 79266482'}]
        new_list = [
            {'type': 'Inc', 'variante': '92-3625175', 'version': '79266482'}]
        separationTypeVarianteVersion(liste_source)
        self.assertEqual(liste_source, new_list)

    def test_renommage_champs(self):
        liste_source = [{'firstname': 'Nicolas', 'lastname': 'FONTAINE'},
                        {'firstname': 'Thomas', 'lastname': 'NOVAREZE'}]

        config = {'firstname': 'prenom', 'lastname': 'nom'}

        result = [{'prenom': 'Nicolas', 'nom': 'FONTAINE'},
                  {'prenom': 'Thomas', 'nom': 'NOVAREZE'}]
        self.assertEqual(renommageChamps(liste_source, config), result)

    def test_change_format_date(self):
        liste_source = [{'date': '2000-01-12'}]
        result = [{'date': '12/01/2000'}]
        changeFormatDate(liste_source)
        self.assertEqual(liste_source, result)
